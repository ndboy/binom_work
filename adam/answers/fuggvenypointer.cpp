/*
	Copyright © Adam Bertalan 2015
*/

#include <iostream>

using namespace std;

void (*fp)(int, int);

void func(int i, int j)
{
	cout << "i: " << i << " j: " << j << endl;
}

int main(int argc, char const *argv[])
{
	int i = 1, j = 2;
	fp = func;
	fp(i,j);
	return 0;
}