/*
	Copyright © Adam Bertalan 2015
*/

#include <iostream>

using namespace std;

void f(int i)
{
	while( i > 0 )
	{
		int x = 0; // a nem static változó minden ciklusban újra megkapja a 0 kezdőértéket
		static int y = 0; // a static változó csak egyszer kapja meg a 0 kezdőértéket
		x++;
		y++;

		cout << x << " " << y << endl;

		i--;
	}
}


int main(int argc, char const *argv[])
{
	f(4);
	return 0;
}