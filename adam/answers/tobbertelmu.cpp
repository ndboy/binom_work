/*
	Copyright © Adam Bertalan 2015
*/
#include <iostream>

using namespace std;

void f1(long i)
{
	cout << "f1 say: " << i << " is long" << endl;
}
void f1(char i)
{
	cout << "f1 say: " << i << " is char" << endl;
}

int main(int argc, char const *argv[])
{
	int i = 4;

	// f1(i); // többértelmű mert nem tudja eldönteni hogy char-rá vagy long-gá konvertálja
	f1((long)i); // többértelműség feloldása típuskényszerítéssel
	
	return 0;
}