Kedves Nándor!

A kötelező olvasmány olvasása ( Stroustrup 191-218 , 221-258 ) közben a következő kérdéseim merültek fel.:

1. 
A 202. oldalon a kódrészben egy függvényen belül egy másik függvény van deklarálva.

void f(int);

void g()
{
	void f(double);
	f(1);
}

Lehetséges akkor hogy nem csak deklarálni tudunk függvényt egy függvényen belül hanem definiálni is?
Illetve lehetséges úgy túlterhelni egy függvényt egy másik függvényen belül hogy 2 azonos nevű függvényem van, és ebből az egyik a függvényen belül van definiálva a másik pedig a függvényen belül csak deklarálva van, viszont a függvényen kívül van definiálva?
Be tudnád ezt nekem mutatni egy példaprogramon keresztül?

2. 
Lehetséges A névtérben deklarálni egy függvényt és B névtérben definiálni? Ekkor mindkét névtérben elérhető a függvény?
Mutasd be egy példán keresztül!

3.
Lehet névtereket egymásba ágyazni? Ha igen akkor a "legmélyebben" lévő névtérből elérhető mondjuk a kettővel felette lévő névtér dupla tartomány-kiválasztó operátorral (::::) ?

Válaszaidat előre is köszönöm!

Üdv: Ádám