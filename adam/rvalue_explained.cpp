#include <iostream>
#include <algorithm>
#include <vector>

class A
{
public:
  A( int in = 0 ) : i(in)
  {
      std::cout << "A class constructor called" << std::endl;
  }

  A(const A& st )
  {
    std::cout << "A class copy constructor called" << std::endl;
  }

  A& operator=( const A& st )
  {
    std::cout << "A class copy assignment called" << std::endl;
    return *this;
  }

  /*move semantics*/
  A& operator=( A&& st )
  {
    std::cout << "A class move assignment called" << std::endl;
    return *this;
  }

  A( A&& st )
  {
    std::cout << "A class move constructor called" << std::endl;
  }

  void raise_i()
  {
    i++;
  }

  void print()
  {
    std::cout << i << std::endl;
  }
private:
  int i;
};
  int main()
  {
    A one; // constructor
    A two; // constructor
    one = two; // copy assignment
    A tree = one; // copy constructor

    std::vector<A> vec;
    vec.push_back( A(5) ); // constructor, move constructor
    vec.at(0) = A(4); // constructor, move assignment


    std::cout << "end main" << std::endl;
    return 0;
  }
