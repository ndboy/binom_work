/*
	Copyright © Nandor Szabo 2015
*/
#include <iostream>
using namespace std;


void f1(int n)
{
	void f11(int n){ //Fordító hibát ír. függvény definíció nem megengedett f1
					// törzsén belül
		cout << n << " most int (f11)" << endl;
	}
	void f11(double n){ // ugyan az a helyzet mint az előzőnél
		cout << n << " most double (f11)" << endl;
	}

	f11(n); //mivel nem tudtuk definiálni egyszer sem ezért meghívni sem tudjuk.
}

void f2(int n)
{
	void f22(int n){ // hasonlan az f1 függvényen belüli függvéyekre 
					//most sem adhatunk meg függvény definíciót
		cout << n << " most int (f22)" << endl;
	}
	void f22(double n);
}

void f22(double n){
	cout << n << " most double (f22)" << endl;
}

int main(void)
{

	f1(1);
	f2(2);

	return 0;
}