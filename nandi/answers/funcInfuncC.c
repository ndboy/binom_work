#include <stdio.h>

/*
	Copyright © Nandor Szabo 2015
*/

//Legutóbbi laborra készülve Bertalan Ádám binom társamnak az a kérdése merült fel, hogy vajon lehet-e definiálni függvényt
// egy másik függvény törzsében. 
//Mint kiderült lehet, gcc gond nélkül fordítja és működik is a program, viszont g++ már hibát ír ki
//függvényen belüli függvénydefiniálásra hivatkozva

void outerF(int n)
{
	void innerF(int n)
	{
		printf("%d\n",n );
	}
	innerF(n);
}

int main(){
	

	outerF(5);
	return 0;
}