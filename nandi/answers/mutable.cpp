#include <iostream>

using namespace std;

class Exmp{
public:

	int  nevaltozz() const
	{
		x++; //Ha x nem mutable-ként lenne definiálva akkor a fordító hibát írna miszerint ez a függvény adatot nem módosíthat
		return x;
	}
private:
	mutable int x = 5;
};



int main()
{
	Exmp e;
	cout << e.nevaltozz() << "   eredeti szám" << endl;
	cout << e.nevaltozz() << "   növelt szám" << endl;
	return 0;
}