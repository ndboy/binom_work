/*
	Copyright © Nandor Szabo 2015
*/

#include <iostream>
using namespace std;

namespace name1{
	void f1(int n);
}

namespace name2{
	f1 void(int n){
		cout << n << endl;
	}
}

int main(void)
{
	name1::f1(1); // name1 névteren belüli f1 függvény hívásakor a fordító
				//hibát ír. Nincs definiálva a függvény.
	return 0;
}