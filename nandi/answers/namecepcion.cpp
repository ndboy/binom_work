/*
	Copyright Nandor Szabo 2015
*/

#include <iostream>
using namespace std;

namespace level1
{
	int x = 0;
	namespace level2
	{
		level1::x = 5;
	}
}


int main(void)
{
	cout << level1::x << endl;
	
	return 0;
}
