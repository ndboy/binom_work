Kedves Bertalan Ádám!

Köszönöm ezuttal isa  kérdéseidet. Íme a válaszok:
1. Igen, lehetséges felhasználni egy operátor túlterhelésben egy már definiáltat. Példaprogrammal ezt be is mutattam(operator.cpp):

#include <iostream>

using namespace std;

class Complex{
public:
	Complex(double re, double im):re(re), im(im){};
	Complex():re(0), im(0){};

	double re, im;

	Complex operator+ (const Complex& com){ // + operátor túlterhelése
		Complex uj;
		uj.re = re + com.re;
		uj.im = im + com.im;

		return uj;
	}

	Complex& operator= (const Complex& com){ // = operátor túlterhelése
		re = com.re;
		im = com.im;
		
	}

	Complex& operator+= (const Complex& com){ // += operátor túlterhelése
		*this = *this + com;
	}

};

int main()
{
	Complex a(1,2);
	Complex b(4,5);

	Complex c;
	c = a + b; // = és + operátor haszbnálata
	cout << "c = a + b" << endl;
	cout << "re(" << c.re << ") im(" << c.im << ")" << endl;
	c += a; // += operátor használata
	cout << "c += a" << endl;
	cout << "re(" << c.re << ") im(" << c.im << ")" << endl;
	cout << "a és b" << endl;
	cout << "re(" << a.re << ") im(" << a.im << ")" << endl;
	cout << "re(" << b.re << ") im(" << b.im << ")" << endl;
	return 0;
}

2. Erre a válaszom annyi lenne, hogy nagyon peciális esetekben van rá szükség. Interneten ezt a választ találtam ugyan erre a kérdésre:
"Let's change the emphasis a bit to:

When should you overload the comma?
The answer: Never.

The exception: If you're doing template metaprogramming, operator, has a special place at the very bottom of the operator precedence list, which can come in handy for constructing SFINAE-guards, etc.

The only two practical uses I've seen of overloading operator, are both in Boost:

Boost.Assign
Boost.Phoenix – it's fundamental here in that it allows Phoenix lambdas to support multiple statements"

forrás: "http://stackoverflow.com/questions/5602112/when-to-overload-the-comma-operator"

3.Tudsz. Viszont a tagfüggvényt csak az utólag definiált osztályban tudod megadni. valamint fontos a sorrend az operátor használatánál. Tehát nem mindegy, hogy Buksi + Laci vagy Laci + Buksi. Példaprogramban is próbáltam illusztrálni(operator2.cpp):

#include <iostream>

using namespace std;

class Ember{
public:
	Ember(int kor):kor(kor){};
	int kor;

	
};

class Kutya{
public:
	Kutya(int kor):kor(kor){};
	int kor;

	int operator+ (const Ember& e){
		int sum_kor = this->kor + e.kor;
		return sum_kor;
	}

};

int main()
{
	Ember Laci(32);
	Kutya Buksi(5);

//	int kor = Laci+Buksi; // ez itt rossz sorrend
	int kor = Buksi+Laci; // jó sorrend

	cout << kor << endl;
	return 0;
}

Remélem érthetőek voltak a válaszaim.

Üdv: Szabó Nándor.
