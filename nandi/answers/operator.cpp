#include <iostream>

using namespace std;

class Complex{
public:
	Complex(double re, double im):re(re), im(im){};
	Complex():re(0), im(0){};

	double re, im;

	Complex operator+ (const Complex& com){ // + operátor túlterhelése
		Complex uj;
		uj.re = re + com.re;
		uj.im = im + com.im;

		return uj;
	}

	Complex& operator= (const Complex& com){ // = operátor túlterhelése
		re = com.re;
		im = com.im;
		
	}

	Complex& operator+= (const Complex& com){ // += operátor túlterhelése
		*this = *this + com;
	}

};

int main()
{
	Complex a(1,2);
	Complex b(4,5);

	Complex c;
	c = a + b; // = és + operátor haszbnálata
	cout << "c = a + b" << endl;
	cout << "re(" << c.re << ") im(" << c.im << ")" << endl;
	c += a; // += operátor használata
	cout << "c += a" << endl;
	cout << "re(" << c.re << ") im(" << c.im << ")" << endl;
	cout << "a és b" << endl;
	cout << "re(" << a.re << ") im(" << a.im << ")" << endl;
	cout << "re(" << b.re << ") im(" << b.im << ")" << endl;
	return 0;
}