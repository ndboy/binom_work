#include <iostream>

using namespace std;

class Ember{
public:
	Ember(int kor):kor(kor){};
	int kor;

	
};

class Kutya{
public:
	Kutya(int kor):kor(kor){};
	int kor;

	int operator+ (const Ember& e){
		int sum_kor = this->kor + e.kor;
		return sum_kor;
	}

};

int main()
{
	Ember Laci(32);
	Kutya Buksi(5);

//	int kor = Laci+Buksi; // ez itt rossz sorrend
	int kor = Buksi+Laci; // jó sorrend

	cout << kor << endl;
	return 0;
}