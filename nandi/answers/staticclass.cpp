#include <iostream>

class Osztaly{
protected:
	static const int x = 5; //konstansként kell megadni a statikus változókat különben nem inicializálhatjuk őket.

};

class Szarm : public Osztaly{
public:
	int b = x;
};

int main()
{
	Szarm s;
	std::cout << s.b << std::endl;
	return 0;
}