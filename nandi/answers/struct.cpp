#include <iostream>

class Exmp{
public:
	struct mine{
		int x = 5;
	};

	mine m; // deklarálnunk kell egy mine tipusú structúrát az osztályunkn belül, hogy az használhassa.
};

int main()
{
	Exmp e;
	std::cout << e.m.x << std::endl;
	return 0;
}